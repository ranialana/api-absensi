<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KaryawanModel extends Model
{
    public $table = 'karyawan';

    public $fillable = [
        'id',
		'nik',
		'nama',
		'email',
        'jenis_kontrak',
        'scan_ktp',
        'npwp',
        'scan_npwp',
        'foto',
        'bank',
        'no_rekening',
        'pin',
        'no_telp'
    ];
}
