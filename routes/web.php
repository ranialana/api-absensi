<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*$app->get('/', function () use ($app) {
    return $app->version();
});
$app->get('/transaksi', 'TransaksiController@index');
$app->post('/transaksi', 'TransaksiController@store');
$app->get('/transaksi-last', 'TransaksiController@last');
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->get('getPegawai', [
    'as' => 'getPegawai', 'uses' => 'KaryawanController@getPegawai'
]);

$router->get('getPegawaiDetail/{id}', [
    'as' => 'getPegawaiDetail', 'uses' => 'KaryawanController@getPegawaiDetail'
]);


$router->put('getAbsenPulang/{id}', [
    'as' => 'getAbsenPulang', 'uses' => 'KaryawanController@getAbsenPulang'
]);


$router->get('getDashboardKiriAtas/{id}', [
    'as' => 'getDashboardKiriAtas', 'uses' => 'KaryawanController@getDashboardKiriAtas'
]);

$router->post('postLogin', [
    'as' => 'postLogin', 'uses' => 'KaryawanController@postLogin'
]);

$router->post('postUpdatePin', [
    'as' => 'postUpdatePin', 'uses' => 'KaryawanController@postUpdatePin'
]);

$router->post('postUpdateBiodata', [
    'as' => 'postUpdateBiodata', 'uses' => 'KaryawanController@postUpdateBiodata'
]);



