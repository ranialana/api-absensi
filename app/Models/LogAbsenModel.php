<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogAbsenModel extends Model
{
    public $table = 'log_absen';

    public $fillable = [
        'id',
		'id_karyawan',
		'waktu_datang',
		'waktu_pulang',
        'selisih_waktu',
        'keterangan',
        'jaldis'
    ];
}
