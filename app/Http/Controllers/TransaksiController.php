<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use DB;

class TransaksiController extends Controller
{
	private $response = array(
		'status' => 'failed',
		'message' => 'Not Found Data!',
		'count'  => 0, 
		'data'   =>null,
	);

    public function index(){
	    $data = Transaksi::all();
	    $this->response['status'] = 'success';
		$this->response['message'] = (count($data) > 0 ? 'Found Data' : 'Not Found Data');
		$this->response['count']  = count($data);
		$this->response['data']  = $data;
	    return response()->json($this->response);
	}

	public function store (Request $request){

		if($request->input('tipe_transaksi') == 'Cr' || $request->input('tipe_transaksi') == 'Db'){
			
			if(!is_numeric($request->input('nilai_transaksi'))){
				$this->response['message'] = 'Value of nilai_transaksi must be number!';
				return response()->json($this->response);
			}
			if(!is_numeric($request->input('saldo'))){
				$this->response['message'] = 'Value of saldo must be number!';
				return response()->json($this->response);
			}
			DB::beginTransaction();
			try {
				$data = new Transaksi();
			    $data->uraian_transaksi = $request->input('uraian_transaksi');
			    $data->tipe_transaksi = $request->input('tipe_transaksi');
			    $data->nilai_transaksi = $request->input('nilai_transaksi');
			    $data->saldo = $request->input('saldo');
			    $data->tanggal_transaksi = $request->input('tanggal_transaksi');
			    if($data->save()){
			    	DB::commit();
					$this->response['status'] = 'success';
					$this->response['message'] = 'Data Transaction Created';
					$this->response['count']  = count($data);
					$this->response['data']  = $data;
					return response()->json($this->response);
			    }
			    
			} catch (Exception $e) {
					DB::rollback();
					$this->response['message'] = $e->getMessage();;
					return response()->json($this->response);
			}
		}else{
			$this->response['message'] = 'Value of tipe_transaksi must be "Cr" or "Db" !';
			return response()->json($this->response);
		}
	    
	}

	public function last(){
	    $data = Transaksi::orderBy('id','desc')->first();
	    $this->response['status'] = 'success';
		$this->response['message'] = (count($data) > 0 ? 'Found Data' : 'Not Found Data');
		$this->response['count']  = count($data);
		$this->response['data']  = $data;
	    return response()->json($this->response);
	}
}
