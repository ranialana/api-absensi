<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KaryawanModel;
use App\Models\LogAbsenModel;
use DB;
use DateTime;
use URL;

class KaryawanController extends Controller
{
    private $response = array(
		'status' => 'failed',
		'data'   =>null,
	);


	public function getPegawai(){
	    $data = KaryawanModel::where('aktif','1')->get();
	    $this->response['status'] = 'success';
		$this->response['data']  = $data;
	    return response()->json($data);
	}

	public function getPegawaiDetail($id){

	    $data = KaryawanModel::where('aktif','1')->where('id',$id)->get();
	    $this->response['status'] = 'success';
		$this->response['data']  = $data;
	    return response()->json($this->response);
	}


	public function postLogin(Request $request){

        $id   	 = $request->input('id');
        $pin     = $request->input('pin');
        $jaldis  = $request->input('jaldis');

        

        $cek_data = KaryawanModel::where('id',$id)->where('pin', md5($pin))->get();

        if(count($cek_data)>0){

        	foreach($cek_data as $row){
        		$id_user   = $row->id;
        		$nama_user = $row->nama;
        	}

            $jamAbsenTerakhir = date('Y-m-d H:i:s');
        	$cek_absen = LogAbsenModel::whereDate('waktu_datang',DATE($jamAbsenTerakhir))->get();
            
            $logAbsen = [];
            if(count($cek_absen)>0){
            	foreach($cek_absen as $ab){
            		if(empty($ab->waktu_pulang)){
            			 $jamAbsenTerakhir = $ab->waktu_datang;
            		}else{
            			 $logAbsen = $ab;
            		}
            	}

            }else{

            	if($jaldis==1){

            		$waktuDatang = date('Y-m-d').' 08:00:00';
                    $waktuPulang = date('Y-m-d').' 17:30:00';

                    $data = [
                		'id_karyawan'    => $id,
                		'waktu_datang'   => $waktuDatang,
                		'waktu_pulang'   => $waktuPulang,
                		'selisih_waktu'  => '09:30:00',
                		'keterangan'     => 'Jaldis'
            		];
 
            		$insert = DB::table('log_absen')->insert($data);

            	}else{

            		$data = [
                		'id_karyawan'    => $id,
                		'waktu_datang'   => $jamAbsenTerakhir,
                		'waktu_pulang'   => '',
                		'selisih_waktu'  => '',
                		'keterangan'     => ''
            		];
 
            		$insert = DB::table('log_absen')->insert($data);

            	}

            }

            $resp['status'] = "OK";
            $this->response['status'] = 'success';
            $this->response['data']['id']     			  = $id;
            $this->response['data']['nama']               = $nama_user;
            $this->response['data']['jamAbsenTerakhir']   = $jamAbsenTerakhir;


        }else {
        	$resp['status'] = "Error";
            $this->response['status'] = 'failed';
            $this->response['data']['pesan'] = 'Username atau password salah';
    	};

	    return response()->json($this->response);
	}


	public function postUpdatePin(Request $request){

		$id                 = $request->input('id');
		$pin_lama  	 	    = $request->input('pin_lama');
    
        $pin_baru       	= $request->input('pin_baru');
        $konfirmasi_pin   = $request->input('konfirmasi_pin');
    
        $cek_data = KaryawanModel::where('pin', md5($pin_lama))->get();

        if($pin_baru==$konfirmasi_pin){

            try { 
        	    $update = DB::table('karyawan')->where('id', $id)->update(['pin' => MD5($pin_baru)]);
                $this->response['status'] = 'success';
                $this->response['data']   = $cek_data;

            }catch(Exception $e) {
                $this->response['status'] = 'failed';
            }
            
        }else{
            $this->response['status'] = 'failed';
            $this->response['data']['pesan']   = 'Pin baru dan konfirmasi pin tidak sesuai';
        }
	    return response()->json($this->response);
	}


	public function postUpdateBiodata(Request $request){

        $id                 = $request->input('id');
		$npwp               = $request->input('npwp');
        $status_hubungan    = $request->input('status_hubungan');
        $jml_tanggungan  	  = $request->input('jml_tanggungan');
        $foto       	 	    = $request->file('foto');
        $file               = $request->file('scan_npwp');

    $cek_data = KaryawanModel::where('id', $id)->get();
    if(count($cek_data)>0){
        if($request->hasFile('scan_npwp')) {

            $base=base_path();
            $filename   = 'npwp_'.$id; 
            $extension  = pathinfo( $_FILES["scan_npwp"]["name"], PATHINFO_EXTENSION ); 
            $basename   = $filename . "." . $extension; 
            $newName='images/'.$basename;

            $source       = $_FILES["scan_npwp"]["tmp_name"];
            $destination  = $base.'/assets/images/'.$basename;
            move_uploaded_file( $source, $destination );
    
		}

        if($request->hasFile('foto')) {
 
            $base=base_path();
            $filename_foto   = 'foto_'.$id; 
            $extension_foto  = pathinfo( $_FILES["foto"]["name"], PATHINFO_EXTENSION ); 
            $basename_foto   = $filename_foto . "." . $extension_foto; 
            $newName_foto='images/'.$basename_foto;

            $source_foto       = $_FILES["foto"]["tmp_name"];
            $destination_foto  = $base.'/assets/images/'.$basename_foto;
            move_uploaded_file( $source_foto, $destination_foto );
    
		}
        

        if($npwp !=''){

            if($request->hasFile('foto')){
                
                try { 
                    
                    $update = DB::table('karyawan')->where('id', $id)->update(['npwp' => $npwp,'status_hubungan'=>$status_hubungan,'jml_tanggungan'=>$jml_tanggungan,'foto'=>$newName_foto]);
                    $this->response['status'] = 'success';
                    $this->response['data']   = $cek_data;

                }catch(Exception $e) {

                    $this->response['status'] = 'failed';
                }

            }else{
                try {   
                    $update = DB::table('karyawan')->where('id', $id)->update(['npwp' => $npwp,'status_hubungan'=>$status_hubungan,'jml_tanggungan'=>$jml_tanggungan]);
                    $this->response['status'] = 'success';
                    $this->response['data']   = $cek_data;
                
                }catch(Exception $e) {

                    $this->response['status'] = 'failed';
                }
            }

            $status = 'success';

        }else{

            if($request->hasFile('foto')){

                try {   
        	        $update = DB::table('karyawan')->where('id', $id)->update(['status_hubungan'=>$status_hubungan,'jml_tanggungan'=>$jml_tanggungan,'foto'=>$newName_foto]);
                    $this->response['status'] = 'success';
                    $this->response['data']   = $cek_data;

                }catch(Exception $e) {
                    $this->response['status'] = 'failed';
                }

            }else{

                try {   
                    $update = DB::table('karyawan')->where('id', $id)->update(['status_hubungan'=>$status_hubungan,'jml_tanggungan'=>$jml_tanggungan]);
                    $this->response['status'] = 'success';
                    $this->response['data']   = $cek_data; 
                } catch(Exception $e) {
                    $this->response['status'] = 'failed';
                }          
            }
            
        }

       
    }else{
        $this->response['status'] = 'failed';
		$this->response['data']['pesan']   = 'Karyawan dengan id tersebut tidak ditemukan';
    }
	    return response()->json($this->response);
	}


	public function getDashboardKiriAtas($id){

        DB::statement("SET sql_mode = '' ");
        $total = 0;
        $query = DB::select("SELECT COUNT(id) as total FROM possible_new_task WHERE id_karyawan = '".$id."' AND tgl ='".date('Y-m-d')."'"); 
        
        foreach($query as $row){
            $total = $row->total;
        }
    
        if($total == 0){
            $tanggal = date('Y-m-d');
            $query = DB::insert("INSERT INTO possible_new_task(id_karyawan,tgl) VALUES ('$id','$tanggal')");

            // possible_new_task
            $hari = date('l');
            $possible_new_task = false;
            $dalam = "";

            // hari senin / selasa / rabu
            if($hari == "Monday" || $hari == "Tuesday" || $hari == "Wednesday"){
                $possible_new_task = true;
                $lusa = date('Y-m-d', strtotime('2 days', strtotime($tanggal)));
                $cek_task = DB::select("SELECT * FROM task_managemen where ditugaskan_ke ='$id' AND DATE(deadline) <= '$lusa' AND (status = 'open' OR status = 'On_Progress' OR status = 'mohon_revisi' )");
                if($hari == "Monday" ){
                    $dalam = "3 hari ke depan ( Senin, Selasa, Rabu ).";            
                }else if($hari == "Tuesday"){
                    $dalam = "3 hari ke depan ( Selasa, Rabu, Kamis ).";            
                }else if($hari == "Wednesday"){
                    $dalam = "3 hari ke depan ( Rabu, Kamis, Jumat ).";            
                }
            }

            if($hari == "Thursday" || $hari == "Friday"){
                $possible_new_task = true;
                $lima_hari_kedepan = date('Y-m-d', strtotime('4 days', strtotime($tanggal)));
                $cek_task =DB::select("SELECT * FROM task_managemen where ditugaskan_ke ='$id' AND DATE(deadline) <= '$lima_hari_kedepan' AND (status = 'open' OR status = 'On_Progress' OR status = 'mohon_revisi' )");            
                if($hari == "Thursday" ){
                    $dalam = "5 hari ke depan ( Kamis, Jumat, Sabtu, Minggu, Senin ).";            
                }else if($hari == "Friday"){
                    $dalam = "5 hari ke depan ( Jumat, Sabtu, Minggu, Senin, Selasa ).";            
                }
            }


            if($hari == "Saturday" || $hari == "Sunday"){
                $possible_new_task = false;
            }

            $total_jam = 0;
              
            foreach($cek_task as $cek){
                $total_jam += $cek->durasi;
            }

            $total_task = count($cek_task);
            $cek_libur = DB::select("SELECT COUNT(id) as libur FROM libur WHERE tgl_libur = '".$tanggal."'"); 

            foreach($cek_libur as $lib){
                $libur_lib = $lib->libur;
            }       

           if($libur_lib > 0){
                $possible_new_task = false;
           }

           if($possible_new_task == true){
            if($total_task <= 1 || $total_jam <= 8){
                $query = DB::select("SELECT * FROM karyawan WHERE id = '".$id."'");            
                
                foreach($query as $q){
                    $role= $q->status_jabatan;
                    $nama_user = $q->nama;
                }

                $jenisCommand = "";
                if($role == 1){
                    $jenisCommand = 'possiblenewtask_direksi';
                }
                if($role == 2){
                    $jenisCommand = 'possiblenewtask_manager';
                }
                if($role == 3){
                    $jenisCommand = 'possiblenewtask_staf';
                }

                $isi_ch = "Nyai Iteung mendeteksi Kang/Teh *".$nama_user."* sepertinya bisa diberikan tugas baru karena antrian pekerjaannya hanya ada *".$total_task."* dengan bobot *".$total_jam." jam* dalam ".$dalam;
                $telegram =  DB::select("SELECT * FROM data_id_telegram WHERE tipe_integrasi = '".$jenisCommand."'");            
                foreach($telegram as $row){
                    $id_chat = $row->id_chat;
                    $this->sen($id_chat, $isi_ch);
                }
            }
          }
        }


        $sekarang = date('Y-m-d');
        $karyawan = DB::select("SELECT nama,jenis_kontrak FROM karyawan WHERE id = '$id'");

        $absen    = DB::select("SELECT id_karyawan, DATE(waktu_datang) as tgl_datang, DATE_FORMAT(waktu_datang, '%H:%i:%s') as jam_datang, DATE(waktu_pulang) as tgl_pulang,

        DATE_FORMAT(waktu_pulang, '%H:%i:%s') as jam_pulang, selisih_waktu FROM log_absen WHERE id_karyawan = '$id' AND DATE(waktu_datang) = '$sekarang'

        AND keterangan != 'Cuti_Reguler' and keterangan != 'Cuti_Rekomendasi'

        ORDER BY jam_datang ASC

        ");
 
        $get_sesi = DB::table('log_absen')->select(DB::raw('id_karyawan, selisih_waktu, DATE(waktu_datang) as tgl_datang, DATE_FORMAT(waktu_datang, "%H:%i:%s") as jam_datang, DATE(waktu_pulang) as tgl_pulang, DATE_FORMAT(waktu_pulang, "%H:%i:%s") as jam_pulang'))->where('id_karyawan',$id)->whereRaw("DATE(waktu_datang) = '".$sekarang."'")->where('keterangan','!=','Cuti_Reguler')->where('keterangan','!=','Cuti_Rekomendasi')->orderBy('jam_datang')->get();

        $thn1 = date('Y-04-16', strtotime('-1 year'));
        $thn2 = date('Y-04-15', strtotime('+1 year'));

        if (date('Y-m-d') < $thn2) {
            if (date('Y-m-d') <=  date('Y-04-15')) {
                $thn1 = date('Y-04-16', strtotime('-1 year'));
                $thn2 = date('Y-04-15');
            } else {
                $thn1 = date('Y-04-16');
                $thn2 = date('Y-04-15', strtotime('+1 year'));
            }
        }


       $cuti = DB::table('log_cuti')->where('id_karyawan',$id)->whereDate('tanggal_cuti', '>=',$thn1)
      ->whereDate('tanggal_cuti', '<=', $thn2)->where('jenis_cuti', 'like','Cuti_%')->orderBy('tanggal_cuti')->get();

       $cutiReguler = DB::table('log_cuti')->where('id_karyawan',$id)->whereDate('tanggal_cuti', '>=',$thn1)
      ->whereDate('tanggal_cuti', '<=', $thn2)->where('jenis_cuti', 'Cuti_Reguler')->get();

       $cutiRekomendasi = DB::table('log_cuti')->where('id_karyawan',$id)->whereDate('tanggal_cuti', '>=',$thn1)
      ->whereDate('tanggal_cuti', '<=', $thn2)->where('jenis_cuti', 'Cuti_Rekomendasi')->get();

       $cutiMenikah = DB::table('log_cuti')->where('id_karyawan',$id)->whereDate('tanggal_cuti', '>=',$thn1)
      ->whereDate('tanggal_cuti', '<=', $thn2)->where('jenis_cuti', 'Cuti_Menikah')->get();

      $reimburse = DB::table('pengajuan')->join('pengajuan_detail', 'pengajuan.id', '=', 'pengajuan_detail.id_pengajuan')->join('karyawan', 'pengajuan.karyawan', '=', 'karyawan.id')->where('pengajuan.status','!=','Draf')->where('pengajuan.status','!=','Selesai')->where('pengajuan.karyawan',$id)->orderBy('pengajuan.id')->get();

      
      $jaldis = DB::select("SELECT jaldis.id as idjaldis,
      jaldis.tunjangan_disetujui as tunjangan_disetujui,
      COUNT(jaldis.id_karyawan) as id_karyawan,
      jaldis.status as status,
      log_jaldis.tanggal as tanggal,
      jaldis.tgl_berangkat as tglberangkat,
      jaldis.tgl_pulang as tglpulang,
      jaldis.tujuan as tujuan,
      jaldis.keperluan as keperluan
      FROM jaldis
      JOIN log_jaldis ON jaldis.id = log_jaldis.id_jaldis
      WHERE jaldis.id_karyawan='$id'
      AND jaldis.status !='Selesai'
      GROUP BY jaldis.id order by jaldis.id asc");

      $awlbulan = date('m');
      $akhirbulan = date('m');
      $tahun = date('Y');
      $awal_periode_bulan_lalu = $tahun . '-' . $awlbulan . '-' . '26';
      $akhir_periode_bulan_lalu = $tahun . '-' . $akhirbulan . '-' . '25';
      if (date('Y-m-d') <= $awal_periode_bulan_lalu) {
          $awl_bulan_sekarang = date('m', strtotime('-2 month'));
          $akhir_bulan_sekarang = date('m', strtotime('-1 month'));
          $tahun = date('Y');
          $awal_periode_bulan_lalu = date('Y-m-26', strtotime('-2 Month'));
          $akhir_periode_bulan_lalu = date('Y-m-25', strtotime('-1 Month'));
          $bulanLalu = $awal_periode_bulan_lalu;
      } elseif (date('Y-m-d') >= date('Y-m-d', strtotime('-1 month', strtotime($akhir_periode_bulan_lalu)))) {
          $awl_bulan_sekarang = date('m', strtotime('-20 Days'));
          $akhir_bulan_sekarang = date('m');
          $tahun = date('Y');
          $awal_periode_bulan_lalu = date('Y-m-26', strtotime('-20 Days'));
          $akhir_periode_bulan_lalu = date('Y-m-25');
          $bulanLalu = date('Y-m-d', strtotime('-30 Days', strtotime($awal_periode_bulan_lalu)));
      }

      

      $dtabsen =DB::select("SELECT karyawan.jenis_kontrak as kontrak,
      log_absen.id as id,
      log_absen.id_karyawan as id_karyawan,
      DAYNAME(log_absen.waktu_datang) as hari,
      log_absen.waktu_datang as waktu_datang,
      log_absen.waktu_pulang as waktu_pulang,
      log_absen.selisih_waktu as selisih_waktu,
      SUM(TIME_TO_SEC(selisih_waktu) / 60) as selisih,
      log_absen.keterangan as keterangan,
      log_absen.jaldis as jaldis
      FROM log_absen
      JOIN karyawan ON log_absen.id_karyawan = karyawan.id
      WHERE log_absen.id_karyawan = '$id'
      AND keterangan != 'Cuti_Reguler' and keterangan != 'Cuti_Rekomendasi'
      AND(DATE(log_absen.waktu_datang) BETWEEN '$bulanLalu' AND '$akhir_periode_bulan_lalu')
      AND DAYNAME(log_absen.waktu_datang) NOT LIKE 'Saturday'
      AND DAYNAME(log_absen.waktu_datang) NOT LIKE 'Sunday'
      AND date(log_absen.waktu_datang) NOT IN (SELECT tgl_libur FROM libur)
      AND date(log_absen.waktu_datang) NOT IN (SELECT tanggal_cuti FROM log_cuti where id_karyawan = '$id')
      GROUP BY date(log_absen.waktu_datang)
      ORDER BY waktu_datang ASC
      ");
      

      $d = new DateTime('-6month');
      $last_six_month_rating = $d->format('Y/m/d');
      $RatingScore = DB::select("SELECT SUM(tr.rating) as totalScore, COUNT(tr.rating) countScore FROM 
        task_report tr, task_managemen tm where (DATE(tm.deadline) >='$last_six_month_rating') and 
        tr.id_task_managemen = tm.id and tm.ditugaskan_ke = '$id'");

      $hadir = DB::select("select * from hadir");
      $event = DB::select("SELECT t1.*, t2.status as isHadir FROM event t1
                left join hadir t2 on t1.id = t2.id_event and t2.id_karyawan = '$id' 
                 order by t1.tutup asc");

        $bobot_prioritas_rendah = 1;
        $bobot_normal = 2;
        $bobot_mendesak = 4;
        $bobot_milestone = 64;
        $pengali_wanprestasi = 8;

        $query_data_task = DB::select("SELECT * from task_managemen where (DATE(deadline) >='$last_six_month_rating') AND ditugaskan_ke='" . $id . "' AND status_ketepatan_waktu = 1");
        $score_ketepatan_waktu = 0;
        $jumlah_ketepatan_waktu = 0;
        $avg_ketepatan_waktu = 0;

        if($query_data_task){
            foreach ($query_data_task as $key => $value) {

                if($value->urgensi == 'prioritas_rendah'){
                    $avg_ketepatan_waktu += $bobot_prioritas_rendah;
                    if($value->status == 'selesai_tepat_waktu'){
                        $jumlah_ketepatan_waktu = $jumlah_ketepatan_waktu + $bobot_prioritas_rendah;
                    }elseif($value->status == 'selesai_terlambat'){
                        $jumlah_ketepatan_waktu = $jumlah_ketepatan_waktu + 0;
                    }elseif( $value->status == 'wanprestasi' || $value->status == 'selesai_wanprestasi' ){
                        $jumlah_ketepatan_waktu = $jumlah_ketepatan_waktu - ( $bobot_prioritas_rendah * $pengali_wanprestasi );
                    }
                }elseif($value->urgensi == 'normal'){
                    $avg_ketepatan_waktu += $bobot_normal;
                    if($value->status == 'selesai_tepat_waktu'){
                        $jumlah_ketepatan_waktu = $jumlah_ketepatan_waktu + $bobot_normal;
                    }elseif($value->status == 'selesai_terlambat'){
                        $jumlah_ketepatan_waktu = $jumlah_ketepatan_waktu + 0;
                    }elseif( $value->status == 'wanprestasi' || $value->status == 'selesai_wanprestasi' ){
                        $jumlah_ketepatan_waktu = $jumlah_ketepatan_waktu - ( $bobot_normal * $pengali_wanprestasi );
                    }

                }elseif($value->urgensi == 'mendesak'){
                    $avg_ketepatan_waktu += $bobot_mendesak;

                    if($value->status == 'selesai_tepat_waktu'){
                        $jumlah_ketepatan_waktu = $jumlah_ketepatan_waktu + $bobot_mendesak;
                    }elseif($value->status == 'selesai_terlambat'){
                        $jumlah_ketepatan_waktu = $jumlah_ketepatan_waktu + 0;
                    }elseif( $value->status == 'wanprestasi' || $value->status == 'selesai_wanprestasi' ){
                        $jumlah_ketepatan_waktu = $jumlah_ketepatan_waktu - ( $bobot_mendesak * $pengali_wanprestasi );
                    }
                }elseif($value->urgensi == 'milestone'){
                    $avg_ketepatan_waktu += $bobot_milestone;

                    if($value->status == 'selesai_tepat_waktu'){
                        $jumlah_ketepatan_waktu = $jumlah_ketepatan_waktu + $bobot_milestone;
                    }else{
                        $jumlah_ketepatan_waktu = $jumlah_ketepatan_waktu - $bobot_milestone;
                    }
                }

                if($avg_ketepatan_waktu > 0){
                    if( $avg_ketepatan_waktu >= $jumlah_ketepatan_waktu ){
                        $score_ketepatan_waktu = ( $jumlah_ketepatan_waktu / $avg_ketepatan_waktu ) * 100;
                    }
                }
            }
        }

        $this->response['status'] = 'success';
        $this->response['data']['karyawan']            = $karyawan;
		$this->response['data']['ssi_masuk']           = $get_sesi;
        $this->response['data']['absen']               = $absen;
        $this->response['data']['cuti']                = $cuti;
        $this->response['data']['cuti_regular']        = $cutiReguler;
        $this->response['data']['cuti_rekomendasi']    = $cutiRekomendasi;
        $this->response['data']['cuti_menikah']        = $cutiMenikah;
        $this->response['data']['reimburse']           = $reimburse;
        $this->response['data']['rating_score']        = $RatingScore;
        $this->response['data']['hadir']               = $hadir;
        $this->response['data']['event']               = $event;
        $this->response['data']['score_ketepatan_waktu']      = $score_ketepatan_waktu;
        $this->response['data']['jaldis']                     = $jaldis;
        $this->response['data']['absen']                      = $dtabsen;
	    return response()->json($this->response);
	}



  function getAbsenPulang($id, Request $request){

     $jam_masuk                 = $request->input('jam_masuk');
     $id_karyawan               = $request->input('id_karyawan');
     $pin                       = $request->input('pin');
     $alasan                    = $request->input('alasan');

     $taskPulang = DB::select("SELECT * from task_managemen where ditugaskan_ke='$id_karyawan' AND (status='open' OR status='On_Progress')");

     $listTaskPulang = DB::select("SELECT id from task_managemen where ditugaskan_ke='$id_karyawan' AND (status='open' OR status='On_Progress')");

      $this->response['data']['taskPulang']                     = $taskPulang;
      $this->response['data']['listTaskPulang']                 = $listTaskPulang;
      return response()->json($this->response);

  }
}
